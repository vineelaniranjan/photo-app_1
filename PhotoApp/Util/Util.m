//
//  Util.m
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import "Util.h"

@implementation Util


//creating button
+(UIButton *)createButton:(NSString *)title withType:(UIButtonType)type withFrame:(CGRect)frame withFontSize:(CGFloat)size withTextColor:(UIColor*)textColor withBackgroundColor:(UIColor *)BackgroungColor withTag:(NSInteger)tag
{
    UIButton *button=[UIButton buttonWithType:type];
    [button setTitle:title forState:UIControlStateNormal];
    [button setFrame:frame];
    button.titleLabel.font=[UIFont systemFontOfSize:size];
    [button setTitleColor:textColor forState:UIControlStateNormal];
    button.tag=tag;
    [button setBackgroundColor:BackgroungColor];
    return button;
}


//creating label
+(UILabel *)createLabel:(NSString *)name withFrame:(CGRect)frame withTextColor:(UIColor *)textColor withBackgroundColor:(UIColor *)backgroundColor withFontSize:(CGFloat)size withLines:(NSInteger)lines
{
    UILabel *label=[[UILabel alloc]initWithFrame:frame];
    label.text=name;
    label.textAlignment=NSTextAlignmentLeft;
    label.textColor=textColor;
    label.backgroundColor=backgroundColor;
    label.font=[UIFont systemFontOfSize:size];
    label.numberOfLines=lines;
    return label;
}

//   making height of label dynamic
+(CGFloat)resizeLableToFitWith:(UILabel*)lbl
{
    float height = [self expectedHeightWithUilable:lbl];
    CGRect newFrame = [lbl frame];
    newFrame.size.height = height;
    [lbl setFrame:newFrame];
    return newFrame.origin.y + newFrame.size.height;
}


//Expected Lable Height
+(CGFloat)expectedHeightWithUilable:(UILabel*)lblObj
{
    [lblObj setNumberOfLines:0];
    [lblObj setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize maximumLabelSize = CGSizeMake(lblObj.frame.size.width,9999);
    
    CGSize expectedLabelSize = [[lblObj text] sizeWithFont:[lblObj font]
                                         constrainedToSize:maximumLabelSize
                                             lineBreakMode:[lblObj lineBreakMode]];
    return expectedLabelSize.height;
}





//creating image view
+(UIImageView *)createImageView:(NSString *)image withFrame:(CGRect)frame withTag:(NSInteger)tag
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    imageView.image=[UIImage imageNamed:image];
    imageView.tag=tag;
    return imageView;
}




//creating scroll view
+(UIScrollView *)createScrollViewWithTag:(NSInteger)tag withFrame:(CGRect)frame withContentWidth:(CGFloat)contentWidth withContentHeight:(CGFloat)contentHeight withVerticalIndicator:(BOOL)vertical withHorizontalIndicator:(BOOL)horizontal withPaging:(BOOL)paging
{
    UIScrollView *scrollView=[[UIScrollView alloc]initWithFrame:frame];
    scrollView.contentSize=CGSizeMake(contentWidth, contentHeight);
    scrollView.scrollEnabled=YES;
    scrollView.showsHorizontalScrollIndicator=horizontal;
    scrollView.showsVerticalScrollIndicator=vertical;
    return scrollView;
}



//creating alert view
+(UIAlertView *)createAlertView:(NSString *)title withMessage:(NSString *)message
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    return alertView;
}




@end
