//
//  ViewController.h
//  PhotoApp
//
//  Created by Shaffiulla Khan on 05/07/13.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIGestureRecognizerDelegate>
{
    UIImageView *image;
    UIView *itemsView;
    NSArray *mainListItemsArr,*secondaryListItemsArray;
    UIButton *editButton,*crossOnTopButton,*crossButton,*backButton;
    NSUndoManager *undoManager;
    BOOL editButtonClicked;
    NSInteger itemSelected;
}


-(void)undoChanges;
-(void)backClicked;
-(void)redoChanges;
-(void)doneChanges;
-(void)crossClicked;
-(void)creatingButtons;
-(void)showIconsAndImagesToAdd;
-(void)itemSelected:(UIButton *)button;
- (void)handlePan:(UIPanGestureRecognizer *)recognizer;
-(void)handleLongPress:(UILongPressGestureRecognizer *)recognizer;


@end
