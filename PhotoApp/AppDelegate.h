//
//  AppDelegate.h
//  PhotoApp
//
//  Created by ShaffiullaKhan on 07/08/13.
//  Copyright (c) 2013 Shaffiulla Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
