//
//  ViewController.m
//  PhotoApp
//
//  Created by Shaffiulla Khan on 05/07/13.
//
//

#import "ViewController.h"
#import "Util.h"
#import <CoreImage/CoreImage.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "FaceDetection.h"
#import "ItemsView.h"


@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor yellowColor];
    undoManager=[[NSUndoManager alloc]init];
    
    mainListItemsArr=[[NSArray alloc]initWithObjects:@"chest1.jpeg", @"ear1.jpeg", @"earrings1.jpeg", @"helmet1.jpeg", @"mouth1.jpeg", @"necklace1.jpeg", @"mask1.jpeg",@"nose1.jpeg", @"weapon1.jpeg", @"eye1.jpeg", @"hair1.jpeg",nil];
    secondaryListItemsArray=[[NSArray alloc]initWithObjects:[[NSArray alloc] initWithObjects:@"chest1.jpeg", @"chest2.jpeg", @"chest3.jpeg", @"chest4.jpeg", @"chest5.jpeg", @"chest6.jpeg", @"chest7.jpeg", @"chest8.jpeg", @"chest9.jpeg", @"chest10.jpeg",nil],[[NSArray alloc] initWithObjects:@"ear1.jpeg", @"ear2.jpeg", @"ear3.jpeg",nil],[[NSArray alloc] initWithObjects:@"earrings1.jpeg", @"earrings2.jpeg", @"earrings3.jpeg", @"earrings4.jpeg",nil],[[NSArray alloc] initWithObjects:@"helmet1.jpeg", @"helmet2.jpeg", @"helmet3.jpeg", @"helmet4.jpeg", @"helmet5.jpeg", @"helmet6.jpeg", @"helmet7.jpeg", @"helmet8.jpeg", @"helmet9.jpeg", @"helmet10.jpeg", @"helmet11.jpeg",@"helmet12.jpeg",@"helmet13.jpeg",nil],[[NSArray alloc] initWithObjects:@"mouth1.jpeg", @"mouth2.jpeg", @"mouth3.jpeg", @"mouth4.jpeg", @"mouth5.jpeg", @"mouth6.jpeg", @"mouth7.jpeg", @"mouth8.jpeg",nil],[[NSArray alloc] initWithObjects:@"necklace1.jpeg", @"necklace2.jpeg", @"necklace3.jpeg", @"necklace4.jpeg", @"necklace5.jpeg", @"necklace6.jpeg",nil],[[NSArray alloc] initWithObjects:@"mask1.jpeg",@"mask2.jpeg",@"mask3.jpeg",@"mask4.jpeg",nil],[[NSArray alloc] initWithObjects:@"nose1.jpeg",@"nose2.jpeg",@"nose3.jpeg",@"nose4.jpeg",@"nose5.jpeg",@"nose6.jpeg",@"nose7.jpeg",nil],[[NSArray alloc] initWithObjects:@"weapon1.jpeg",@"weapon2.jpeg",@"weapon3.jpeg",@"weapon4.jpeg",@"weapon5.jpeg",@"weapon6.jpeg",@"weapon7.jpeg",@"weapon8.jpeg",nil],[[NSArray alloc] initWithObjects:@"eye1.jpeg", @"eye2.jpeg", @"eye3.jpeg",nil],[[NSArray alloc] initWithObjects:@"hair1.jpeg",@"hair2.jpeg",nil],nil];
    FaceDetection *faceDetection=[[FaceDetection alloc]init];
    image=[faceDetection faceDetector:@"facedetectionpic.jpg" withDelegate:self withFrame:CGRectMake(0,0, 320, 460)];
    
       
    [self creatingButtons];
}

-(void)creatingButtons
{
    editButton=[Util createButton:@"Edit" withType:UIButtonTypeCustom withFrame:CGRectMake(0,420,80, 40) withFontSize:20 withTextColor:[UIColor colorWithRed:.343 green:.16453 blue:.25434 alpha:1] withBackgroundColor:[UIColor colorWithRed:.87565 green:.93232 blue:.94353 alpha:1] withTag:1];
    [editButton addTarget:self action:@selector(showIconsAndImagesToAdd) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:editButton];
    
    UIButton *undoButton=[Util createButton:@"Undo" withType:UIButtonTypeCustom withFrame:CGRectMake(80,420, 80, 40) withFontSize:20 withTextColor:[UIColor colorWithRed:.343 green:.16453 blue:.25434 alpha:1] withBackgroundColor:[UIColor colorWithRed:.87565 green:.93232 blue:.94353 alpha:1] withTag:1];
    [undoButton addTarget:self action:@selector(undoChanges) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:undoButton];
    
    UIButton *redoButton=[Util createButton:@"Redo" withType:UIButtonTypeCustom withFrame:CGRectMake(160,420, 80, 40) withFontSize:20 withTextColor:[UIColor colorWithRed:.343 green:.16453 blue:.25434 alpha:1] withBackgroundColor:[UIColor colorWithRed:.87565 green:.93232 blue:.94353 alpha:1] withTag:1];
    [redoButton addTarget:self action:@selector(redoChanges) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:redoButton];
    
    UIButton *doneButton=[Util createButton:@"Done" withType:UIButtonTypeCustom withFrame:CGRectMake(240,420, 80, 40) withFontSize:20 withTextColor:[UIColor colorWithRed:.343 green:.16453 blue:.25434 alpha:1] withBackgroundColor:[UIColor colorWithRed:.87565 green:.93232 blue:.94353 alpha:1] withTag:1];
    [doneButton addTarget:self action:@selector(doneChanges) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneButton];
    
    
    crossOnTopButton=[Util createButton:@"" withType:UIButtonTypeCustom withFrame:CGRectMake(145, 0, 30, 20) withFontSize:11 withTextColor:nil withBackgroundColor:nil withTag:NO];
    [crossOnTopButton setBackgroundImage:[UIImage imageNamed:@"cross.jpeg"] forState:UIControlStateNormal];
    crossOnTopButton.hidden=YES;
    crossOnTopButton.userInteractionEnabled=NO;
    [self.view addSubview:crossOnTopButton];
}

-(void)showIconsAndImagesToAdd
{
    editButton.userInteractionEnabled=NO;
    editButtonClicked=YES;
    ItemsView *itemView=[[ItemsView alloc]init];
    itemsView=[itemView createViewWithItems:mainListItemsArr withDelegate:self];
    crossButton=[Util createButton:@"" withType:UIButtonTypeCustom withFrame:CGRectMake(255,30, 20, 20) withFontSize:10 withTextColor:nil withBackgroundColor:nil withTag:100];
    [crossButton setBackgroundImage:[UIImage imageNamed:@"cross.jpeg"] forState:UIControlStateNormal];
    [crossButton addTarget:self action:@selector(crossClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:crossButton];
}

-(void)itemSelected:(UIButton *)button
{
    if (editButtonClicked)
    {
        itemSelected=button.tag;
          editButtonClicked=NO;
        backButton=[Util createButton:@"" withType:UIButtonTypeCustom withFrame:CGRectMake(45,30, 20, 20) withFontSize:10 withTextColor:nil withBackgroundColor:nil withTag:101];
        [backButton setBackgroundImage:[UIImage imageNamed:@"cross.jpeg"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backButton];
        [itemsView removeFromSuperview];
        ItemsView *itemView=[[ItemsView alloc]init];
        itemsView=[itemView createViewWithItems:[secondaryListItemsArray objectAtIndex:button.tag-1] withDelegate:self];
    }
    else
    {
        [itemsView removeFromSuperview];
        [crossButton removeFromSuperview];
        [backButton removeFromSuperview];
        editButton.userInteractionEnabled=YES;
        UIImageView *itemImageView=[Util createImageView:[[secondaryListItemsArray objectAtIndex:itemSelected-1]objectAtIndex:button.tag-1] withFrame:CGRectMake(130, 150, 60, 40) withTag:NO];
        itemImageView.userInteractionEnabled = YES;
        UIPanGestureRecognizer *panRecognizer =[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
        UILongPressGestureRecognizer *longPressRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
        itemImageView.gestureRecognizers = @[longPressRecognizer,panRecognizer];
        [self.view addSubview:itemImageView];

    }
    
           
}
- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    crossOnTopButton.hidden=NO;
    CGPoint translation = [recognizer translationInView:recognizer.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x+translation.x, recognizer.view.center.y+ translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:recognizer.view];
    if ((recognizer.view.center.x+translation.x<175||recognizer.view.center.x+translation.x>145)&&(recognizer.view.center.y+ translation.y<20))
    {
        [recognizer.view removeFromSuperview];
         crossOnTopButton.hidden=YES;
    }
    if(recognizer.state == UIGestureRecognizerStateEnded)
    {
       crossOnTopButton.hidden=YES;
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)recognizer
{
    if (recognizer.state==UIGestureRecognizerStateBegan)
    {
        [[recognizer.view layer]setBorderWidth:1];
        [[recognizer.view layer]setBorderColor:[UIColor blackColor].CGColor];
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinched:)];
        pinchGesture.delegate = self;
        [recognizer.view addGestureRecognizer:pinchGesture];
        NSLog(@"Show boxes");
    }
}

         
      
      
-(void)crossClicked
{
    [itemsView removeFromSuperview];
    [crossButton removeFromSuperview];
    [backButton removeFromSuperview];
    editButtonClicked=NO;
    editButton.userInteractionEnabled=YES;
}
-(void)backClicked
{
    [backButton removeFromSuperview];
    [itemsView removeFromSuperview];
    editButtonClicked=YES;
    ItemsView *itemView=[[ItemsView alloc]init];
    itemsView=[itemView createViewWithItems:mainListItemsArr withDelegate:self];

}




-(void)undoChanges
{
    [undoManager undo];
    //[undoManager registerUndoWithTarget:self selector:@selector(resetImage) object:pageView.canvas.image];
   // undoRedoStackIndex--;
}

-(void)redoChanges
{
    [undoManager redo];
    //undoRedoStackIndex++;
    
}


-(void)doneChanges
{
    
}






- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
   
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
