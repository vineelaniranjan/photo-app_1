//
//  main.m
//  PhotoApp
//
//  Created by ShaffiullaKhan on 07/08/13.
//  Copyright (c) 2013 Shaffiulla Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
