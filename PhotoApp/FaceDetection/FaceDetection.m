//
//  FaceDetection.m
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import "FaceDetection.h"
#import <QuartzCore/QuartzCore.h>

@implementation FaceDetection
@synthesize delegate;


-(void)markFaces:(NSArray *)imageArray
{
    UIImageView *facePicture = [imageArray objectAtIndex:0];
    CGRect frameBounds = CGRectFromString([imageArray objectAtIndex:1]);
    // draw a CI image with the previously loaded face detection picture
    CIImage* image = [CIImage imageWithCGImage:facePicture.image.CGImage];
    
    // create a face detector - since speed is not an issue we'll use a high accuracy
    // detector
    CIDetector* detector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:[NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy]];
    
    // create an array containing all the detected faces from the detector
    NSArray* features = [detector featuresInImage:image];
    
    // we'll iterate through every detected face.  CIFaceFeature provides us
    // with the width for the entire face, and the coordinates of each eye
    // and the mouth if detected.  Also provided are BOOL's for the eye's and
    // mouth so we can check if they already exist.
    for(CIFaceFeature* faceFeature in features)
    {
        // get the width of the face
        // 1. get the width by dividing Image Actual width by width that is manually set to fit the image on screen
        CGFloat faceWidth =frameBounds.size.width/facePicture.bounds.size.width;
        CGFloat faceHeight = frameBounds.size.height/facePicture.bounds.size.height;
        // 2. divide this width by faceFeature Width
        CGFloat width = faceFeature.bounds.size.width / faceWidth;
        CGFloat height  =faceFeature.bounds.size.height / faceHeight;
        // Get Coordinate which by dividing get the actaul position of given image
        CGFloat x =frameBounds.size.width/facePicture.bounds.size.width;
        CGFloat y = frameBounds.size.height/facePicture.bounds.size.height;
        
        
        // create a UIView using the bounds of the face
        UIView* faceView = [[UIView alloc] initWithFrame:CGRectMake(faceFeature.bounds.origin.x/x, faceFeature.bounds.origin.y/y, width, height)];
        
        // add a border around the newly created UIView
        faceView.layer.borderWidth = 1;
        faceView.layer.borderColor = [[UIColor redColor] CGColor];
        
        
        // add the new view to create a box around the face
        [self.delegate.view addSubview:faceView];
        
        if(faceFeature.hasLeftEyePosition)
        {
            // create a UIView with a size based on the width of the face
            UIView* leftEyeView = [[UIView alloc] initWithFrame:CGRectMake(0,0, width*0.3, width*0.3)];
            // change the background color of the eye view
            [leftEyeView setBackgroundColor:[[UIColor blueColor] colorWithAlphaComponent:0.3]];
            // set the position of the leftEyeView based on the face
            [leftEyeView setCenter:CGPointMake(faceFeature.leftEyePosition.x/x, faceFeature.leftEyePosition.y/y)];
            // round the corners
            leftEyeView.layer.cornerRadius = width*0.15;
            // add the view to the window
//            [self.delegate.view addSubview:leftEyeView];
        }
        
        if(faceFeature.hasRightEyePosition)
        {
            // create a UIView with a size based on the width of the face
            UIView* leftEye = [[UIView alloc] initWithFrame:CGRectMake(0,0, width*0.3, width*0.3)];
            // change the background color of the eye view
            [leftEye setBackgroundColor:[[UIColor blueColor] colorWithAlphaComponent:0.3]];
            // set the position of the rightEyeView based on the face
            [leftEye setCenter:CGPointMake(faceFeature.rightEyePosition.x/x, faceFeature.rightEyePosition.y/y)];
            // round the corners
            leftEye.layer.cornerRadius = width*0.15;
            // add the new view to the window
//            [self.delegate.view addSubview:leftEye];
        }
        
        if(faceFeature.hasMouthPosition)
        {
            // create a UIView with a size based on the width of the face
            UIView* mouth = [[UIView alloc] initWithFrame:CGRectMake(0,0, width*0.4, width*0.4)];
            // change the background color for the mouth to green
            [mouth setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.3]];
            // set the position of the mouthView based on the face
            [mouth setCenter:CGPointMake(faceFeature.mouthPosition.x/x, faceFeature.mouthPosition.y/y)];
            // round the corners
            mouth.layer.cornerRadius = width*0.2;
            // add the new view to the window
//            [self.delegate.view addSubview:mouth];
        }
    }
}

-(UIImageView *)faceDetector:(NSString *)imageName withDelegate:(id)delegateObj withFrame:(CGRect)imageFrame
{
    self.delegate=delegateObj;
    // Load the picture for face detection
    UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    CGRect frameBounds = image.bounds;
    image.backgroundColor=[UIColor redColor];
    
    // Change the image size to fit the screen
    image.frame =imageFrame;
    //    image.frame = CGRectMake(0, 0, 320, 480);
    // Draw the face detection image
    [self.delegate.view addSubview:image];
    NSArray *array = [[NSArray alloc] initWithObjects:image,NSStringFromCGRect(frameBounds), nil];
    // Execute the method used to markFaces in background
    [self performSelectorInBackground:@selector(markFaces:) withObject:array];
    
    // flip image on y-axis to match coordinate system used by core image
//    [image setTransform:CGAffineTransformMakeScale(1, -1)];
    
    // flip the entire window to make everything right side up
   // [SharedAppDelegate.window setTransform:CGAffineTransformMakeScale(1, -1)];
    
    return image;
}




@end
