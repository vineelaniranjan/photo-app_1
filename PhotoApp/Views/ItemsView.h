//
//  ItemsView.h
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface ItemsView : NSObject
{
    ViewController *delegate;
}
@property(strong,nonatomic)ViewController *delegate;
-(UIView *)createViewWithItems:(NSArray *)items withDelegate:(id)delegateObj;
@end
