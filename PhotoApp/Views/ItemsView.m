//
//  ItemsView.m
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import "ItemsView.h"
#import "Util.h"

@implementation ItemsView
@synthesize delegate;

-(UIView *)createViewWithItems:(NSArray *)items  withDelegate:(id)delegateObj
{
    self.delegate=delegateObj;
    UIScrollView *itemsScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(45,50, 230,175 )];
    [itemsScrollView setBackgroundColor:[UIColor colorWithRed:.0123 green:.12323 blue:.16453 alpha:.8]];
       
    CGFloat x=7.5,y=10;
    for (int i=0; i<[items count]; i++)
    {
        UIButton *itemButton=[Util createButton:@"" withType:UIButtonTypeCustom withFrame:CGRectMake(x, y, 50,50) withFontSize:10 withTextColor:nil withBackgroundColor:nil withTag:i+1];
        [itemButton setBackgroundImage:[UIImage imageNamed:[items objectAtIndex:i]] forState:UIControlStateNormal];
        [itemButton addTarget:self.delegate action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];
        [itemsScrollView addSubview:itemButton];
        x=x+55;
        if (x>200)
        {
            x=7.5;
            y=y+55;
        }
    }
    if (x!=7.5)
    {
        y=y+55;
    }
    itemsScrollView.contentSize=CGSizeMake(230,y+5 );
    [self.delegate.view addSubview:itemsScrollView];
    return itemsScrollView;
}




@end
